from xml.etree.ElementTree import Element
import explore_dnb as dnb

# Ici vos fonctions dédiées aux interactions

# ici votre programme principal
def programme_principal():
    
    liste_options_principale = ["Chargez un fichier","Liste des fichiers chargé","Menu des fonctions","Quitter"]
    liste_options_fonctions = ["Taux réussite","Meilleur","Meilleurs taux de réussite","Pire taux de réussite","Totale admis et présent","Filtre session","Filtre département","Filtre college","Taux réussite global pour une session","Moyenne taux réussite college session","Meilleur collège",'Liste session',"Plus longue période d'amélioration","Est bien triée","Fusionner résultat"]
    liste_Nomfichier_charger = []
    liste_fichier_charger = []
    nom_fichier_a_charger = ""
    fichierAct = ""
    while True :
        menu_principale(liste_options_principale,"Bienvenue dans le menu")
        nb_option_choisis = input(f"choisisser une option entre [1-{len(liste_options_principale)}]\n")
        if nb_option_choisis == "1" :
            print(f"\nVous avez choisis de charger un fichier")
            nom_fichier_a_charger = input("Entrez le nom d'un fichier\n")
            liste_fichier_charger.append(menu_charger(nom_fichier_a_charger,liste_Nomfichier_charger))
            liste_Nomfichier_charger.append(nom_fichier_a_charger)


            print("Appuyer sur entrée pour continuer")
            input()
        if nb_option_choisis == "2" :
            print(f'\nVoici la liste des fichier chargé :')
            for i in range(len(liste_Nomfichier_charger)) :
                print(f'{i+1} -> {liste_Nomfichier_charger[i]}')


            print("\nAppuyer sur entrée pour continuer")
            input()
        if nb_option_choisis == "3" : 

            menu_fonction(liste_options_fonctions,"Menu des fonctions",liste_fichier_charger,liste_Nomfichier_charger)
            
            print("\nAppuyer sur entrée pour continuer")
            input()
        if nb_option_choisis == "4" :
            print(f"\nVous avez choisis l'option {liste_options_principale[int(nb_option_choisis)-1]} \nMerci et aurevoir")
            break
        
    

def menu_principale(liste_options,titre):

    print(f'+-{"-"*len(titre)}-+')
    print(f'| {titre} |')
    print(f'+-{"-"*len(titre)}-+')
    nb = 1
    for i in range(len(liste_options)) :
        print(f'{nb} -> {liste_options[nb-1]}')
        nb += 1
    return nb



def menu_fonction(liste_options,titre,liste_fichier_charger,liste_Nomfichier_charger):
    print(f'+-{"-"*len(titre)}-+')
    print(f'| {titre} |')
    print(f'+-{"-"*len(titre)}-+')
    nb = 1
    for i in range(len(liste_options)) :
        print(f'{nb} -> {liste_options[nb-1]}')
        nb += 1
    nb_option_fonctions_choisis = input(f"choisisser une option entre [1-{len(liste_options)}]\n")



    if nb_option_fonctions_choisis == "1" :
        #taux réussite
        print(f'\nEntrer un tuple de la forme : session,nom,département,nombre_present,nombre_admis') 
        fichierAct = input()
        try :
            print(f'\nLe taux de réussite du tuple demandé est de :{"%.2f" % dnb.taux_reussite(fichierAct.split(","))} %')
        except :
            print("Le tuple demandé n'est pas valide")



    if nb_option_fonctions_choisis == "2" :
        print(f'\n Entrer le premier tuple de la forme : session,nom,département,nombre_present,nombre_admis')
        mot1 = input()
        print(f'\n Entrer le deuxième tuple de la forme : session,nom,département,nombre_present,nombre_admis')
        mot2 = input()
        #Meilleur entre deux
        try :
            if dnb.meilleur(mot1.split(","),mot2.split(",")) == True :
                print(f'\n Le tuple avec le taux le plus élever est le premier avec {"%.2f" % dnb.taux_reussite(mot1.split(","))} %')
            else: 
                print(f'\n Le tuple avec le taux le plus élever est le deuxième avec {"%.2f" % dnb.taux_reussite(mot2.split(","))} %')
        except :
            print("Le tuple demandé n'est pas valide")
        


    if nb_option_fonctions_choisis == "3" :
        #Meilleurs dans une liste
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f'\nLe Meilleurs taux de réussite du fichier {fichierAct} est de :{"%.2f" % dnb.meilleur_taux_reussite(liste_fichier_charger[i][0])} %')



    if nb_option_fonctions_choisis == "4" :
        #Pire taux dans une liste
        print(f'{liste_Nomfichier_charger}\n"Entree le nom du fichier que vous voulez"') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f'\nLe pire taux de réussite du fichier {fichierAct} est de :{"%.2f" % dnb.pire_taux_reussite(liste_fichier_charger[i][0])} %')



    if nb_option_fonctions_choisis == "5" :
        #total admis liste
        print(f'{liste_Nomfichier_charger}\n"Entree le nom du fichier que vous voulez"') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f"Le nombre total d'elève admis et present dans ce fichier est de {dnb.total_admis_presents(liste_fichier_charger[i][0])}:")



    if nb_option_fonctions_choisis == "6" :
        #filtre session
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\n"Entree la session que vous voulez') 
        session = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f'\n La sous-liste du fichier demandé est : {dnb.filtre_session(liste_fichier_charger[i][0],session)}')
    


    if nb_option_fonctions_choisis == "7" :
        #filtre session
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\n"Entree le département que vous voulez') 
        departement = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f'\n La sous-liste du fichier demandé est : {dnb.filtre_departement(liste_fichier_charger[i][0],departement)}')



    if nb_option_fonctions_choisis == "8" :
        #filtre college
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\nEntree le nom de college que vous voulez') 
        college = input()
        print(f'\nEntree le département que vous voulez') 
        departement = input()

        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f'\n La sous-liste du fichier demandé est : {dnb.filtre_college(liste_fichier_charger[i][0],college,departement)}')



    if nb_option_fonctions_choisis == "9" :
        #taux reussite global
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\n"Entree la session que vous voulez') 
        session = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f"\n Le taux de réussite global sur l'année {session} est de  : {'%.2f' % dnb.taux_reussite_global(liste_fichier_charger[i][0],session) }%")
        

    if nb_option_fonctions_choisis == "10" :
        #moyenne_taux_reussite_college
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\nEntree le nom de college que vous voulez') 
        college = input()
        print(f'\nEntree le département que vous voulez') 
        departement = input()
        try :
            
            for i in range(len(liste_fichier_charger)) :
                if dnb.moyenne_taux_reussite_college(liste_fichier_charger[i][0],college,departement) != None :
                    if liste_fichier_charger[i][1] == fichierAct :
                        print(f"\n Le taux de réussite global du college {college} est de  : {'%.2f' % dnb.moyenne_taux_reussite_college(liste_fichier_charger[i][0],college.upper(),departement) }%")
                else :
                    print("Les informations données ne sont pas valide, veuillez réessayer.")
        except :
            print("Les informations données ne sont pas valide, veuillez réessayer.")


    if nb_option_fonctions_choisis == "11" :
        #Meilleur college
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        print(f'\n"Entree la session que vous voulez') 
        session = input()
        
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                tuple_fichier = dnb.meilleur_college(liste_fichier_charger[i][0],session)
                print(f"\n Le meilleurs collège pour la session, {session} est le collège {tuple_fichier[0]} du département {tuple_fichier[1]}")


    if nb_option_fonctions_choisis == "12" :
        #liste_sessions
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f"\nVoici la liste des session du fichier {fichierAct} : \n{dnb.liste_sessions(liste_fichier_charger[i][0])}\n")


    if nb_option_fonctions_choisis == "13" :
        #plus longue période d'amélioration
        
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                print(f"\nVoici la plus longue période d'amélioration  : \n{dnb.plus_longe_periode_amelioration(liste_fichier_charger[i][0])}\n")


    if nb_option_fonctions_choisis == "14" :
        #est bien triée
        print(f'{liste_Nomfichier_charger}\nEntree le nom du fichier que vous voulez') 
        fichierAct = input()
        for i in range(len(liste_fichier_charger)) :
            if liste_fichier_charger[i][1] == fichierAct :
                if dnb.est_bien_triee(liste_fichier_charger[i][0]) == True :
                    print("Le fichier est bien triée")
                else :
                    print("Le fichier n'est pas triée")


 

    if nb_option_fonctions_choisis == "15" :
        #fusionnée résultat
        try :
            print(f'{liste_Nomfichier_charger}\nEntree le nom du premier fichier à fusionner') 
            fichierAct = input()
            print(f'{liste_Nomfichier_charger}\nEntree le nom du deuxième fichier à fusionner') 
            fichierAct2 = input()
            for i in range(len(liste_fichier_charger)) :
                if liste_fichier_charger[i][1] == fichierAct :
                    f1 = liste_fichier_charger[i][0]
                if liste_fichier_charger[i][1] == fichierAct2 :
                    f2 = liste_fichier_charger[i][0]
            
            print(f"\nLa fusion des deux résultats est  \n{dnb.fusionner_resultats(f1,f2)}\n")
        except :
            print("Erreur dans les noms des fichiers")




def menu_charger(nom_fic,liste_nom_fichier) :
    try :
        liste_fichier = []
        fic=open(nom_fic, 'r')
        fic.readline()
        tuple_ajout = ()
        for elem in fic  :
            tuple_ajout = elem.split(",")
            liste_fichier.append((tuple_ajout[0],tuple_ajout[1],tuple_ajout[2],tuple_ajout[3],tuple_ajout[4][0:-1]))
            tuple_ajout = ()
        fic.close()
        return (liste_fichier,nom_fic)
    except :
        return "Erreur dans le nom du fichier"




programme_principal()
